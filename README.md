# click-bot

My son wanted a "auto-clicker" for roblox 

* 10 second count down
* random delay times between clicks
* clicks a little faster than a human could

## run from cmd

### show help output
```
click-bot.exe --help
```

### click 9001 times
```
click-bot.exe -c 9001
```


## click-bot.exe sha512sum

7145b8476ea15fd0c967ca71629da6ad084e74b6e02576de5627162d1390b7463d94a3387e1bf545e90a485f8c73917c4414ed5278300e48a8355ccf54bf3a1e  click-bot.exe

